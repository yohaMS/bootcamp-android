## Bootcamp Android



# Programación Funcional

Paradigma de programación que busca que el código sea facil de comprender y probar, su objetivo es delegar el control de flujo de condiciones a fuciones.

Para conocer la cantidad de número payores a 10 en la lista 

    
        List<Integer> numeros = List.of(18, 6, 4, 15, 55, 78, 12, 9, 8);
    

Se implementaría de la siguiente manera

    
        Long result = numeros.stream().filter(num -> num > 10).count();
    

## Funciones puras 
Misma entrada misma salida. *No hay efectos secundarios*
  - Efectos secundarios: Cambio fuera de la función llamada, se evita en la Programación funcional

    
        constaddAndSend=(x1,x2)=>{
            return x1 + x2
        }
    

## Composición de funciones
Combina 2 ó mas funciones, para ejecutarlas en secuencia


    > helloWord()
    > setName()

    > println(helloWord(setName()))


## Estado compartido
Puede ser una variable, objeto o espacio de memoria compartido


    Materia materia = new Materia()
    Usuario usuario = new Usuario()

    usuario.materia = materia


## Mutabilidad
Objeto inmutable, no puede ser modificado una vez asignado su valor (Constantes, en Kotlin lo que definimos como val)


    var caracteres "ABC"
    var caracteresMutados caracteres.toLowerCase()


# Programación Orientada a Objetos (OOP)

Paradigma basado en objetos definidos, independiente y puedenn ser usados con otros objetos. 
Los objetos combinan propiedades y atributos que describen al objeto.

    
        Marcador{
            String color
            String tamaño
            Strinng tintaDisponible
            String material

            marcador(String color){
                this.color = color
            }

        }
    

*La clase marcador se convierte en nuestro objeto y sus atributos como color, tamaño hace parte de las propiedades del objeto y sus metodos son todo aquello que puede hacer el objeto.*

# Clase
Documento donde se definen las propiedades de un objeto
 - Instannciar: Se crea objeto a partir de una clase

# Propiedades de un objeto
Colección de datos que el objeto guarda, es distinta entre objetos 

# Método 
Funciones del objeto (Alterar,Retornar y calculos u operaciones)
 - Método constructor: Se ejecuta cuando se instancia, debe tener un proposito único y ser limitado a inicialización de propiedades (No volverlos complicados) 

Funndamnetos de la POO
 # Herencia 
 Su propósito es reutilizar código, permite defiir una clase a partir de la definnició de otra existente. El padre (base) va de lo general a lo especifico
  - Ejemplo:  animal -> mamimero -> perro. 

 # Encapsulación
 Permite agrupar atributos, que solo permiten ser modificados mediante operaciones definidas en el objeto
    - Publico
    - Protegido
    - Privado
 # Polimorfismo 
 Cada instancia del objeto, maneeja un único. 


 # SOLID
 Principio que busca las mejores practicas para que el código sea limpio, refactorizable, agil y adaptativo 

 ## Sigle - Principio de responsabilidad única
 Las clases deberan tener responsabilidad unica

 ## Open - Principio abierto cerrado
 Las clases deben de estar abiertas por extensión, pero cerradas por modificación

        Interface Figure { 
            area()
        }

        Class Circle implements Figure {
            private Double radious;

            public Double area() { 
                return ( Math.PI * radious) / 2
            }

        Class Rectangle implements Figure { 
            private Double x;
            private Double y;

            rectangle(Double x, Double y) {
                this.x = x;
                this.y = y;
            }

            public Double area() {
                return x * y;
            }
        }

        Class Main() { 
            private Circle circle;
            private Rectangle rect;

            main() { 
                circle = new Circle(10);
                rect = new Rectangle(10,10);
                print(calcArea( circle));
                print(calcArea( rect));
            }

            public Double calcArea(Figure figure) {    
                return figure.area()

                // no sigue el principio open close 
                if(  figure instanceof Circle) {
                    return ( Math.PI * figure.getRadious() ) / 2
                }
                else if (figure instanceof Rectangle ) {
                    return ( figure.getX() * figure.getY()); 
            }
        }


 ## Liskov
 Declara que una subclase debe ser sustituible por su superclase, y si al hacer esto, el programa falla, estaremos violando este principio

        public Double calcArea(Figure figure) {    
                return figure.area() 
        }
        print(calcArea( rect));

 ## Interface - Principio de segregación de interfaz
Establece que los clientes no deberían verse forzados a depender de interfaces que no usan

        Interface Animal { 
            move() 
            sound() 
        }

        interface Vertebrates extends animal {

        }

        Interface Mammals extends vertebrates{ 
            breastfeed();
        }


        class Gato implements mamals { 

        }


 ## Dependency - Principio Inyección de dependencia
Establece que las dependencias deben estar en las abstracciones, no en las concreciones

        Interface Animal { 
            move() 
            sound() 
        }

        interface Vertebrates extends animal {

        }

        Interface Mammals extends vertebrates{ 
            breastfeed();
        }


        class Cat implements mamals { 

        }

        class Dog implements mamals { 

        }



        class Zoo() { 
            List<Animal> animals;      

            animal.add(new Cat())
            animal.add(new Dog())  


        }


 # GIT


 ## git config

Permite establecer valores como usuario, correo, formatos, entre otros


    git config --global user.email "monsalve@gmail.com"


 ## git  init

 Inicializa el repositorio git
 
 
    git init
 

 ##  git clone

Crea copia del repositorio GIT


    git@gitlab.com:yohaMS/bootcamp-android.git


 ##  git add

Añade cambios de archivos en el directorio de ensayo a al index


    git add README.md


 ##  git rm

 Elimina archivos del index y del directorio de ensayo
 
    git rm filename
 

## git commit 

Toma todos los cambios escritos en el index, crea un nuevo objeto de confirmación que apunta a él y establece la rama para que apunte a esa nueva confirmación


    git commit -m ‘committing added changes’


## git status 

Muestra el estado de los objetos modificados, el index vs el directorio de trabajo

    git status


##  git branch

Consulta las ramas del repositorio y señala la rama en la que se ecuentra

    git branch


##  git merge

Fusiona una o más ramas con otra rama activa

    git merge nuevaRama

##  git reset 

Vuelve al ultimol estado 

    git reset --hard HEAD


## git push

Envía todos los objetos modificados localmente al repositorio remoto

    git push --set-upstream origin semana0

